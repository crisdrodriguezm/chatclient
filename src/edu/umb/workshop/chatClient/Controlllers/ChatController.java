package edu.umb.workshop.chatClient.Controlllers;

import edu.umb.workshop.chatClient.Views.MainWindow;
import edu.umb.workshop.chatClient.Models.ChatModel;
import edu.umb.workshop.chatClient.Views.RegistrationForm;

public class ChatController implements Runnable
{

    private final ChatModel model;
    private final MainWindow chatWindow;
    private RegistrationForm registrationForm;

    public ChatController() {
        model = new ChatModel();
        chatWindow = new MainWindow(this);
        registrationForm = new RegistrationForm(this);
    }

    public void init() {
        registrationForm.setVisible(true);
        model.init();
    }

    public void sendMessage(String mensaje) {
        model.sendMessage(mensaje);
    }

    public void register(String nombre, String nickname, String password,
            String confirmacionPassword) {
        if (model.register(nickname, nombre, password, confirmacionPassword)) {
            registrationForm.setVisible(false);
            registrationForm = null;
            chatWindow.setVisible(true);
            getMessages();
        } else {
            chatWindow.showMessage("Error", model.getErrorMessage());
        }
    }

    public void getMessages() {
        Thread hiloModelo = new Thread(model);
        hiloModelo.start();
        Thread controllerThread = new Thread(this);
        controllerThread.start();
    }

    @Override
    public void run() {

        while (true) {
            StringBuilder mensajes = new StringBuilder();
            model.getMessages().forEach((mensaje) -> {
                mensajes.append(mensaje).append("\n");
            });
            chatWindow.getChatTextArea().setText(mensajes.toString());
        }
    }
}
